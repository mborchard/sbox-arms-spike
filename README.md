# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Contents
### Proof of Concepts
* NewRelic Synthetic Monitor [current]
    * Lambda: 
        * `mborchard_api_gateway_rest_api_lambda`
* Postgres `aws_lambda` Extension
    * Lambda: 
        * `aws_lambda_ext_lambda`
* Cloudwatch Events
    * Lambda: 
        * `cloudwatch_events_time_trigger_lambda`

### Lambda Functions
* current_lambda
* other_lambda
* other_lambda

### Resources
* API Gateway
## Summary of Setup
### .env Configuration
* 
* 
* 
### Dependencies
### Database Configuration
* 
* 
* 

## Testing
~ **From highest to lowest levels** ~

### NewRelic ###
* 
* 
* 

### API Gateway REST API ###
* 
* 
* 

### API Gateway Components ###
* 
* 
* 

### Lambda ###
* 
* 
* 

### RDS ###
* Connect to DB: 
    ```
    psql -h mborchard-liquibase-test.c5byr2jj2sir.us-east-1.rds.amazonaws.com -d postgres -U mborchard
    ```
* 
* 


## Deployment Instructions
* 
* 
* 

### Who do I talk to? ###

* Mason Borchard
    * Contact info:
        * Email: mborchard@k12.com
        * Slack: @Mason U. Borchard (they/them) 