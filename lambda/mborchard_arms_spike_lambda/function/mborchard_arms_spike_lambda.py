# Version with API Gateway payload sent via EventBridge schedule
import json
import urllib3
import logging
import os
import psycopg2
import sys
import time
import newrelic.agent
import ssm_parameter_store

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Create a dictionary containing all SSM Parameters relevant to this function
store = ssm_parameter_store.SSMParameterStore(prefix='/mborchard/arms_spike')

# RDS Credentials 
rds_host  = store['rds_host']
rds_username = store['rds_username']
rds_user_pwd = store['rds_user_pwd']
rds_db_name = store['rds_db_name']

# webhook url
slack_webhook_url = store['slack_webhook_url']


def handler(event, context):
    t0 = time.perf_counter()

    ## Part 1: Connect to RDS and query DB
    logger.info("-------- Part 1 --------")

    try:
        conn_string = "host=%s user=%s password=%s dbname=%s" % \
                        (rds_host, rds_username, rds_user_pwd, rds_db_name)

        conn = psycopg2.connect(conn_string)
    except:
        logger.error("ERROR: Could not connect to Postgres instance:\nhost: %s\ndatabase: %s\nusername: %s" % \
            (rds_host, rds_db_name, rds_username))   
        sys.exit() 

    # Identify the query attached to the event payload
    if event["query"]:
        query = event["query"]
    else:
        return { 'statusCode': 200, 'error': 'No query or query params found in event\n event: %s' % str(event)}

    # Connect to Postgres and run query identified in the payload

    conn = psycopg2.connect(conn_string)

    with conn.cursor() as cur:
        cur.execute(query)
        raw = cur.fetchone()
        count = raw[0]

    # If the query returns anything, store the count in 'results' string which will be sent to Slack

    # Check if the query returns a count above the given threshold 
    # FIXME: refactor to get the count from payload insstead of hard-coding it
    if count > 10:
            results = str(count) + " API message failures returned from query ```" + query + "```" 
            logger.info({ 'statusCode': 200, 'errorCount': count})
    
    # If threshold is not met, return a 200 response but do not post to Slack
    else:
        logger.info({ 'statusCode': 200, 'errorCount': count})
        results = "<1 API message failures returned from query: ```" + query + "```"
        t1 = time.perf_counter()
        executionTime = t1 - t0
        return { 'statusCode': 200, 'body': event, 'message': results, 'errorCount': count, 'executionTime': executionTime }

        
    ## Part 2: Send Slack Notification
    logger.info("-------- Part 2 --------")

    data = {"text": "Alert from database " + str(rds_db_name) + "\n\n" + str(results)} 

    # Calling Slack API ---
    # TODO: Discuss whether or not we want to trigger alerts to Slack from both: 
        # 1. A single Lambda invocation whenever a single query returns count > threshold) 
        # &
        # 2. The NewRelic Alert Policy whenever a series of queries from n Lambda invocations meets a predetermined alert condition
    # One benefit I see of posting a notifiation in Slack whenever an individual query returns a count > threshold is that it is instantaneous, specific, and reliable
    # However, there may be a way to make the NewRelic Alert policy fire:
        # 1. Instantly even when just a single query returns a count > threshold and/or 
        # 2. With details in the custom payload sent from NewRelic showing the queries involved and their respective error counts
        
    http = urllib3.PoolManager()
    req = http.request("POST", slack_webhook_url, body = json.dumps(data), headers = {"Content-Type": "application/json"})
    
    try:
        req.read()
        logger.info("SUCCESS: Message posted!")
        logger.info("Message posted to Slack:\n%s" % (data))
    except urllib3.exceptions.HTTPError as e:
        logger.error("Request failed: %d %s" % (e.code, e.reason))
    except urllib3.exceptions.ConnectTimeoutError as e:
        logger.error("Server connection failed: %s" % (e.reason))

    t1 = time.perf_counter()
    executionTime = t1 - t0

    logger.info({ 'statusCode': 200, 'body': event.keys, 'executionTime': executionTime })

    ## Part 3: Send metrics to NewRelic
    logger.info("-------- Part 3 --------")

    # Send custom event and parameters to NewRelic
    newrelic.agent.record_custom_event("ArmsMonitoringEvent", {
        "ArmsMonitoringQuery": query,
        "ArmsMonitoringErrorCount": count
    })

    newrelic.agent.add_custom_parameter('ArmsMonitoringQuery', query)
    newrelic.agent.add_custom_parameter('ArmsMonitoringErrorCount', count)
 
    return {
        'statusCode': 200,
        'body': json.dumps(event),
        'message': results, 
        'errorCount': count, 
        'query': query,
        'executionTime': executionTime
    }