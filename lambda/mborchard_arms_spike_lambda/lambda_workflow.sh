#!/bin/bash

# # Workflow for updating and testing a lambda

# delete existing deployment package:
rm -rf mborchard_arms_spike_lambda.zip

# create new deployment package:
zip -r mborchard_arms_spike_lambda.zip .

# update function with new deployment package
aws lambda update-function-code --region "us-east-1" --function-name "mborchard_arms_spike_lambda" --zip-file fileb://mborchard_arms_spike_lambda.zip

# aws lambda invoke --function-name mborchard_arms_spike_lambda  filter_output.txt

# invoke script to view/debug the logs
aws lambda invoke --function-name mborchard_arms_spike_lambda out --log-type Tail \
--query 'LogResult' --output text |  base64 -d

# # Other scripts

# deploy lambda
aws lambda create-function --region "us-east-1" \
    --function-name "mborchard_arms_spike_lambda"       \
    --zip-file fileb://mborchard_arms_spike_lambda.zip  \
    --handler "mborchard_arms_spike_lambda.handler"     \
    --role "${role_arn}"             \
    --runtime "python3.8"            \
    --timeout 60                     \
    --vpc-config "${vpc_config}"     

# create ssm paramter
aws ssm put-parameter --cli-input-json '{"Type": "SecureString", "KeyId": "alias/aws/ssm", "Name": "SlackWebHookURL", "Value": "'"$WEBHOOK_URL"'"}'

# get logs
chmod -R 755 get_logs.sh

aws lambda invoke --function-name mborchard_arms_spike_lambda --cli-binary-format raw-in-base64-out --payload '{"key": "value"}' out
sed -i'' -e 's/"//g' out
sleep 15
aws logs get-log-events --log-group-name /aws/lambda/mborchard_arms_spike_lambda --log-stream-name $(cat out) --limit 5   